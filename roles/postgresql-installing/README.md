PosqtreSQL installin role
=========

Роль для установки и настройки PostgreSQL <br />
<br />
<br />
Роль устанавливает PostgreSQL, через нее можно вносить правки в конфиг через соответствующие переменные в default/main ( группа помечена как "postgresql.conf генерация" ). <br />
Так же через роль производится создание новых баз и пользователей, и инициализация кластера. <br />

-- В данный момент сделана только установка stolon. Инициализацию надо делать вручную -- <br />
-- Вариант инициализации stolon кластера скриптом можно взять от сюда https://github.com/deepdivenow/ansible-role-stolon/blob/master/templates/cluster_init_command.sh.j2 -- <br />


Role Variables
------------

Теги роли: <br />
pgsql_db - запускает таски на создание БД и пользователя <br />
stolon - запускает установку stolon <br />
pgadmin - запускает таски на установку и настройку pgadmin <br />
<br />
<br />
PGAdmin после установки доступен по адерсу http://ip/pgadmin4/<br />
PGAdmin: если нужно будет указать пути программ в настройках, то указывать /usr/pgsql-12/bin/<br />